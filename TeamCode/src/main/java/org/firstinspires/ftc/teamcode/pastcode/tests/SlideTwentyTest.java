package org.firstinspires.ftc.teamcode.pastcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.pastcode.RepresentoClass;

@Disabled
@Autonomous
public class SlideTwentyTest extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        RepresentoClass r = new RepresentoClass(this);
        r.startGyro();
        waitForStart();
        r.slide(1, 20);
        r.stopMotor();
    }
}
