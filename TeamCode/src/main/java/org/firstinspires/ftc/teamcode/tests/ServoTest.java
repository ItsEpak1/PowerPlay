package org.firstinspires.ftc.teamcode.tests;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

@TeleOp
public class ServoTest extends LinearOpMode {

    private Servo clawServo;

    @Override
    public void runOpMode() {
        clawServo = hardwareMap.get(Servo.class, "claw");

        telemetry.addData("Status", "Initialized");
        telemetry.update();

        waitForStart();

        while (opModeIsActive()) {
            if(gamepad1.y) {
                clawServo.setPosition(1);
            } else if (gamepad1.a){
                clawServo.setPosition(0);
            }

            telemetry.addData("Claw servo position: ", clawServo.getPosition());
        }

        clawServo.setPosition(0);
    }
}